import React, { Component } from 'react';
import Chat from './chat';
import './chats.scss';

class Chats extends Component {

  constructor(props) {
    super(props);
    this.chatsRef = React.createRef();
    this.scrollToBottom = this.scrollToBottom.bind(this);
  }

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom() {
    this.chatsRef.scrollIntoView({behavior: 'smooth'});
  }

  render() {
    return (
      <div className="Chats">
        {this.props.messages.map(message => (
          <Chat user={this.props.user} message={message} key={message.id} />
        ))}

        <div ref={el => { this.chatsRef = el }}>
        </div>
      </div>
    );
  }
}

export default Chats;