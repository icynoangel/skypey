import React from 'react';
import Header from './header';
import Chats from './chats';
import Input from './../common/input';
import './chat-window.css';

const ChatWindow = (props) => {
  const { user, messages, activeUser, sendMessage } = props;


  let currentChatMessages;

  if (activeUser.id) {
    currentChatMessages = messages[activeUser.id] || [];
  } else {
    currentChatMessages = messages[activeUser.groupId] || [];
  }

  const send = (text) => {
    if (activeUser.id) {
      sendMessage(text, user.id, activeUser.id);
    } else {
      sendMessage(text, user.id, activeUser.groupId);
    }
  }

  return (
    <div className="ChatWindow">
      <Header user={activeUser} />
      <Chats messages={currentChatMessages} user={user} />
      <Input onSubmit={send} />
    </div>
  );
};

export default ChatWindow;