import React from 'react';

const Chat = (props) => {
  const { message, user } = props;

  const isUserMessage = message.senderId === user.id;

  return (
    <div  className={`Chat ${isUserMessage ? "is-user-msg" : ""} `}>

      <div className="Chat__senderId">
        {message.senderId}
      </div>

      <div className={`Chat__message ${isUserMessage ? "is-user-msg" : ""}`}>
        {message.text}
      </div>
    </div>
  );
};

export default Chat;