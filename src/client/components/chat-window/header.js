import React from 'react';
import './header.css';

const Header = (props) => {
  
  let chatName;

  if(props.user.id){
    chatName = props.user.id;
  } else {
    chatName = props.user.groupName;
  }

  return ( 
    <header className="Header">
      <h2 className="Header__name">{chatName}</h2>
    </header>
  );
};

export default Header;