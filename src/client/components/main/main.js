import React, { Component } from 'react';
import './main.css';
import Connect from '../connect/connect';
import Empty from '../empty/empty';
import GroupChatForm from '../groupChats/groupChatForm';
import ChatWindow from '../chat-window/chat-window';

class Main extends Component {

  renderMainContent(){
    // console.log(this.props);

    if(this.props.groupChatWindow){
      return <GroupChatForm users={this.props.users} user={this.props.user} createGroup={this.props.createGroup}/>;
    }

    if (!this.props.activeUser) {
      return <Empty user={this.props.user} />;
    } else {
      return <ChatWindow {...this.props} />
    }
    
  }
  
  render(){
    return (
      <main className="main">
      {this.props.user ? this.renderMainContent() :
         <Connect connect={this.props.connect} ui={this.props.ui} />}
      </main>
    );
  }
}

export default Main;