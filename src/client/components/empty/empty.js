import React from 'react';
import './empty.scss';

const Empty = (props) => {

    return (
        <div className="empty">
            <h1 className="empty__name">
                Welcome, {props.user.id}
            </h1>

            <button className="empty__btn">
                Start a conversation
            </button>

        </div>
    );
};

export default Empty;