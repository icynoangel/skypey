import React from 'react';
import classNames from 'classnames';
import './connection.scss';

const Connection = (props) => {
    const { connected } = props.ui.connection;

    const classes = classNames(
        {
            'connection__info' : true,
            'error' : !connected,
            'success' : connected
        }
    );

    return (
        <div className="connection">
            <div className= {classes}>
                {connected === true ? 'Connected' : 'Disconnected'}
            </div>
        </div>
    );
}

export default Connection;