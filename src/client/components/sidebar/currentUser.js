import React from 'react';
import {
    IconSettings, Avatar
} from '@salesforce/design-system-react';
import './currentUser.scss';

const CurrentUser = (props) => {
    return (
        <IconSettings iconPath="/icons">
            <div className="currentUser">

                <div className="currentUser__avatar">
                    <div className="currentUser__online"></div>
                    {/* <img className="currentUser__image" src="http://placekitten.com/50/50" alt="avatar" /> */}
                    <Avatar variant="user" label={props.currentUser.id.charAt(0)} size="large" />
                </div>
                <h1 className="currentUser__name">
                    {props.currentUser.id}
                </h1>
            </div>
        </IconSettings>

    );
};

export default CurrentUser;