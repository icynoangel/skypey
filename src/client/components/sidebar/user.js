import React, { Component } from 'react';
import './user.css';

class User extends Component {

  constructor(props) {
    super(props);
    this.onClick = this.onClick.bind(this);
    this.newMessagesIcon = this.newMessagesIcon.bind(this);
    this.getUserName = this.getUserName.bind(this);
    this.state = {
      show: false
    };
  }

  onClick() {
    this.props.setActiveUser(this.props.user);
    this.setState({ show: false });
  }

  newMessagesIcon() {
    if ((!this.props.activeUser ||
      (this.props.user.id && this.props.user.id !== this.props.activeUser.id) ||
      (this.props.user.groupId && this.props.user.groupId !== this.props.activeUser.groupId)
    ) && this.state.show === true) {
      return (
        <div className="user__icon"></div>
      );
    }
    return null;
  }

  componentDidUpdate(prevProps) {
    if ((!this.props.activeUser ||
      (this.props.user.id && this.props.user.id !== this.props.activeUser.id) ||
      (this.props.user.groupId && this.props.user.groupId !== this.props.activeUser.groupId)
    )) {
      if (this.props.messages !== prevProps.messages) {
        this.setState({ show: true });
      }
    }
  }

  getUserName() {
    if (this.props.user.id) {
      return this.props.user.id;
    }
    return this.props.user.groupName;
  }

  render() {
    return (
      <div className="user" onClick={this.onClick} >
        <div className="user__details">
          <p className="user__details-name">{this.getUserName()}</p>
          {this.newMessagesIcon()}
        </div>

      </div>
    );
  }
}

export default User;