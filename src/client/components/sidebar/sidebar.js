import React from 'react';
import User from './user';
import CurrentUser from './currentUser';
import './sidebar.scss';
import _ from 'lodash';

const Sidebar = (props) => {

  const { user, users, setActiveUser, messages, activeUser, showGroupChatWindow } = props;

  const renderConnectedUsers = () => {


    return <div className="usersList" >
      {_.map(users, (user) => {
        
        let msg;
        if (user.id) {
          msg = messages[user.id] ? messages[user.id].length : 0
        } else {
          msg = messages[user.groupId] ? messages[user.groupId].length : 0
        }

        return (
          <User user={user} key={user.id || user.groupId}
            setActiveUser={setActiveUser}
            messages={msg}
            activeUser={activeUser}
          />
        )
      })}
    </div>
  };

  const initGroupChat = () => {
    showGroupChatWindow(true);
  }

  if (user) {
    return (

      <aside className="sidebar">
        <CurrentUser currentUser={user} key={user.id} />

        <div className="showGroupChat"
          onClick={() => initGroupChat()}>
          <img src="https://img.icons8.com/color/30/000000/chat.png" alt="chatIcon" />
          <span className="showGroupChat__text"> New Group Chat </span>

        </div>

        {renderConnectedUsers()}
      </aside>
    );
  }
  return null;
};

export default Sidebar;