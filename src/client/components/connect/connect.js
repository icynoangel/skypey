import React, { Component } from 'react';
import MessageInput from '../common/input';
import {
  IconSettings, AlertContainer, Alert, Icon
} from '@salesforce/design-system-react';
import './connect.scss';

class Connect extends Component {

  constructor(props) {
    super(props);
    this.connect = this.connect.bind(this);
  }

  connect(value) {
    this.props.connect(value);
  }

  renderError() {
    return <h5 className="error">{this.props.ui.error.message}</h5>;
  }

  render() {
    return (
      <IconSettings iconPath="/icons">
        <div className="connect">

          {this.props.ui.error && this.props.ui.error.type === 'custom-error' ?
            <AlertContainer>
              <Alert
                icon={<Icon category="utility" name="error" />}
                labels={{
                  heading:
                    this.props.ui.error.message,
                }}
                variant="error"
              />
            </AlertContainer>
            //  <h5 className="error">{this.props.ui.error.message}</h5> 
            : null
          }

          <h4 className="connect__label">
            Please enter a name to connect
        </h4>

          <div className="connect__input">
            <MessageInput onSubmit={this.connect} placeholder="Enter name to connect" />
          </div>
        </div>
      </IconSettings>
    );
  }

}

export default Connect;