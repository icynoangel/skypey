import { Component } from 'react';
import ws from '../../services/ws';

class WS extends Component {

  constructor(props) {
    super(props);
    this.handleMessage = this.handleMessage.bind(this);
    this.handleError = this.handleError.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.retryConnect = this.retryConnect.bind(this);
  }

  render() {
    return null;
  }

  handleMessage(msg) {
    // act on any message type
    switch (msg.type) {
      case 'connected':
        this.props.setLoggedIn(msg.user);
        break;
      case 'message':
        this.props.receiveMessage(msg, this.props.user);
        break;
      case 'users':
        this.props.receiveUsers(msg.users, this.props.user);
        break;
      case 'custom-error':
        this.props.setError(msg);
        break;
      // case 'error':
      //   this.props.setConnectionStatus(false);
      //   break;
      default:
        break;
    }
  }

  getLastCreatedGroup() {
    const users = this.props.users;
    const len = users.length;
    for (let i = len - 1; i > 0; i--) {
      if (users[i].groupInitiator === this.props.user.id && !users[i].initialized) {
        return users[i];
      }
    }
    return null;
  }

  componentDidUpdate(prevProps) {
    if (this.props.users.length !== prevProps.users.length) {
      const lastGroup = this.getLastCreatedGroup();
      if (lastGroup) {
        this.props.setActiveUser(lastGroup);
      }
    };
  }

  handleOpen() {
    this.props.setConnectionStatus(true);
  }

  handleClose() {
    this.retryConnect();
  }

  handleError(event) {
    this.props.setError(event);
    this.props.setConnectionStatus(false);
  }

  retryConnect() {
    window.setTimeout(() => {
      ws.connect();
    }, 1000);
  }

  componentDidMount() {
    ws.connect();
    ws.injectCallbacks(this.handleMessage, this.handleError, this.handleOpen, this.handleClose);
  }

  componentWillUnmount() {
    ws.disconnect();
  }
}

export default WS;