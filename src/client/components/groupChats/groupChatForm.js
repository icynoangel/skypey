import React from 'react';
import './groupChatForm.scss';

class GroupChatForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedUsers: [props.user.id],
      groupInitiator: props.user.id,
      groupName: '',
      error: ''
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.setGroupName = this.setGroupName.bind(this);
    this.createGroup = this.createGroup.bind(this);

  };

  renderSelectUsers() {
    const users = this.props.users;

    return users.map((user, i) => {
      if (user.id) {
        return (
          <div key={i} className="groupForm__users_value">
            <label htmlFor={user.id}>
              {user.id}
              <input
                type="checkbox"
                name={user.id}
                onChange={this.handleInputChange}
                value={this.state.selectedUsers[user.id]}
              />
            </label>
          </div>
        )
      }
      return null;
    });
  }

  createGroup() {

    if (!this.state.groupName.trim()) {
      this.setState({
        error: 'Please provide a group name.'
      });
      return;
    }

    if (!this.state.selectedUsers.length) {
      this.setState({
        error: 'Please select users.'
      });
      return;
    }

    this.setState({ error: '' }, () => {
      this.props.createGroup(this.state.groupName, this.state.selectedUsers, this.state.groupInitiator);
    });

  }

  handleInputChange(event) {
    const isChecked = event.target.checked;
    const item = event.target.name;

    if (isChecked) {
      this.setState({ 'selectedUsers': [...this.state.selectedUsers, item] });
    } else {
      this.setState((state) => {
        const newState = [...state.selectedUsers];
        newState.splice(state.selectedUsers.indexOf(item), 1);
        return { selectedUsers: newState };
      })
    }
  }

  setGroupName(event) {
    this.setState({
      groupName: event.target.value
    });
  }

  render() {
    return (
      <div className="groupForm">

        <h2 className="groupForm__header">New Group Chat</h2>

        <div className="error">
          {this.state.error}
        </div>

        <div className="groupForm__name">
          <label htmlFor="group-name">
            <input className="groupForm__name-input"
              type="text"
              name="group-name"
              value={this.state.groupName}
              placeholder="Group Chat Name:"
              onChange={this.setGroupName}
              autoFocus />
          </label>
        </div>

        <div className="groupForm__users">
          Available users:
            {this.renderSelectUsers()}
        </div>

        <button onClick={this.createGroup} className="groupForm__button"> &#8594; </button>
      </div>
    );
  }
}

export default GroupChatForm;