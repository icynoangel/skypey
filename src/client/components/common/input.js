import React, { Component } from 'react';
import './input.css';

class Input extends Component {
   
   static defaultProps = {
      placeholder: 'Type a message here'
   };

  constructor(props){
    super(props);

    this.state = {
      value: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e){
    this.setState({value: e.target.value});
  }
  
  onSubmit(e){
    e.preventDefault();
    this.props.onSubmit(this.state.value);
    this.setState({value: ''});
  }

  render(){
    return (
      <form className="input-container" onSubmit={this.onSubmit} >
        <input
            className="input-container__input"
            onChange={this.onChange}
            value={this.state.value}
            placeholder={this.props.placeholder}
        />
      </form>
    );
  }
}


export default Input;