
class WS {

  constructor() {
    
    this.ws = null;

    this.addListeners = this.addListeners.bind(this);
    this.removeListeners = this.removeListeners.bind(this);
    this.sendMessage = this.sendMessage.bind(this);
    this.handleMessage = this.handleMessage.bind(this);
    this.handleError = this.handleError.bind(this);
    this.handleOpen = this.handleOpen.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.connect = this.connect.bind(this);
    this.disconnect = this.disconnect.bind(this);
    this.injectCallbacks = this.injectCallbacks.bind(this);
  }

  injectCallbacks(callback, errorCallback, openCallback, closeCallback) {
    this.callback = callback;
    this.errorCallback = errorCallback;
    this.openCallback = openCallback;
    this.closeCallback = closeCallback;
  }

  handleOpen(event) {
    console.log('connected WS', event);
    this.openCallback();
  }

  handleClose(event) {
    console.log('closed WS', event);
    this.closeCallback();
  }

  handleError(event) {
    console.log("WebSocket error", event);
    this.errorCallback(event);
  }

  handleMessage(event) {
    console.log('message', event.data);
    this.callback(JSON.parse(event.data));
  }

  sendMessage(message) {
    this.ws.send(JSON.stringify(message));
  }

  addListeners() {
    this.ws.addEventListener('open', this.handleOpen);
    this.ws.addEventListener('error', this.handleError);
    this.ws.addEventListener('message', this.handleMessage);
    this.ws.addEventListener('close', this.handleClose);
  }

  removeListeners() {
    if(this.ws) {
      this.ws.removeEventListener('open', this.handleOpen);
      this.ws.removeEventListener('error', this.handleError);
      this.ws.removeEventListener('message', this.handleMessage);
      this.ws.removeEventListener('close', this.handleClose);
    }
  }

  connect() {
    this.removeListeners();
    this.ws = new WebSocket('ws://localhost:8000');
    this.addListeners();
  }

  disconnect() {
    this.ws.send(JSON.stringify({
      type: 'disconnect'
    }));
  }
}

export default new WS();