import user from './user';
import users from './users';
import activeUser from './activeUser';
import messages from './messages';
import ui from './ui';
import groupChatWindow from './groupChatWindow';
import appInitialState from '../app-state/app-initial-state';

const reducer = (state = appInitialState, action) => {
  return {
    user: user(state.user, action),
    activeUser: activeUser(state.activeUser, action),
    users: users(state.users, action),
    messages: messages(state.messages, action),
    ui: ui(state.ui, action),
    groupChatWindow: groupChatWindow(state.groupChatWindow, action)

  };
}

export default reducer;