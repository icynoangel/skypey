import { SET_ACTIVE_USER, SET_USERS, SHOW_GROUPCHAT_WINDOW } from '../actions/actions'
import _ from 'lodash';

const activeUser = (state, action) => {
  switch (action.type) {
    case SET_ACTIVE_USER:
      return action.user;
    case SHOW_GROUPCHAT_WINDOW:
      return null;
    case SET_USERS:
      if (!_.find(action.users, state)) {
        return null;
      }
      return state;

    default:
      return state;
  }
};

export default activeUser;