import {
  SET_USERS, SET_ACTIVE_USER
} from './../actions/actions';

const users = (state, action) => {
  switch (action.type) {
    case SET_USERS:
      return action.users.filter((user) => {
        return user.id !== action.user.id;
      });
    case SET_ACTIVE_USER:
      return state.map((user) => {
        if (action.user.groupId && user.groupId === action.user.groupId && !user.initialized) {
          return {
            ...user,
            initialized: true
          };
        }
        return user;
      });
    default:
      return state;
  }
}

export default users;