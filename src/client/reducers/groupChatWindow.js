import { SHOW_GROUPCHAT_WINDOW, SET_ACTIVE_USER } from '../actions/actions'

const groupChatWindow = (state, action) => {
  switch (action.type) {
    case SHOW_GROUPCHAT_WINDOW:
      return action.status;
    case SET_ACTIVE_USER:
      return false;
    default:
      return state;
  }
};

export default groupChatWindow;