import { SET_ERROR, SET_CONNECTION_STATUS } from '../actions/actions'

const ui = (state, action) => {
  switch (action.type) {
    case SET_ERROR:
      return {
        error: action.event,
        connection: state.connection
      };
    case SET_CONNECTION_STATUS:
      return {
        error: state.error,
        connection: {
          connected: action.status
        }
      };
    default:
      return state;
  }
};

export default ui;