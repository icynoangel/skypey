import {
  SET_LOGGED_IN
} from './../actions/actions';

const user = (state, action) => {
  switch(action.type) {
    case SET_LOGGED_IN:
      return action.user;
    default:
      return state;
  }
}

export default user;