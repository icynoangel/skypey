
import { RECEIVE_MESSAGE, SEND_MESSAGE } from '../actions/actions';

const messages = (state, action) => {

  let newState;
  switch (action.type) {
    case SEND_MESSAGE:
      newState = { ...state };
      if (!newState[action.message.receiverId]) {
        newState[action.message.receiverId] = [];
      }

      newState[action.message.receiverId] = [...newState[action.message.receiverId], action.message];
      return newState;

    case RECEIVE_MESSAGE:
      newState = { ...state };

      if (action.message.receiverId === action.user.id) {
        if (!newState[action.message.senderId]) {
          newState[action.message.senderId] = [];
        }
        newState[action.message.senderId] = [...newState[action.message.senderId], action.message];
      } else {
        if(!newState[action.message.receiverId]){
          newState[action.message.receiverId] = [];
        }

        newState[action.message.receiverId] = [...newState[action.message.receiverId], action.message];
      }

      return newState;

    default:
      return state;
  }
}

export default messages;