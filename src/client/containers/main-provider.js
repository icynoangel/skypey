import { connect } from 'react-redux';
import Main from '../components/main/main';
import { sendMessage } from '../actions/message-actions';
import { login } from '../actions/user-actions';
import { createGroup } from '../actions/groupChat-actions';

const mapStateToProps = (state) => {
  return {
    user: state.user,
    messages: state.messages,
    activeUser: state.activeUser,
    ui: state.ui,
    users: state.users,
    groupChatWindow: state.groupChatWindow,
  };
}

const mapActionToProps = (dispatch) => {
  return {
    connect: (id) => {
      login(id);
    },
    createGroup:(groupName, groupUsers, groupInitiator) => {
      createGroup(groupName, groupUsers, groupInitiator);
    },
    sendMessage: (text, userId, activeUserId) => {
      dispatch(sendMessage(text, userId, activeUserId));
    }
  };
}


export default connect(mapStateToProps, mapActionToProps)(Main);