import { connect } from 'react-redux';
import { setActiveUser } from '../actions/user-actions';
import { showGroupChatWindow } from "../actions/groupChat-actions";
import Sidebar from '../components/sidebar/sidebar';

const mapStateToProps = (state) => {
  return {
    user: state.user,
    users: state.users,
    messages: state.messages,
    activeUser : state.activeUser,
    groupChatWindow: state.groupChatWindow

  };
}

const mapActionsToProps = (dispatch) => {
  return {
    setActiveUser: (user) => {
      dispatch(setActiveUser(user));
    },
    showGroupChatWindow: (status) => {
      dispatch(showGroupChatWindow(status));
    }
  };
}



export default connect(mapStateToProps, mapActionsToProps)(Sidebar);