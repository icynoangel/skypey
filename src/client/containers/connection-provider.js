import { connect } from 'react-redux';
import Connection from '../components/connection/connection';

const mapStateToProps = (state) => {
  return {
    ui: state.ui
  };
}

export default connect(mapStateToProps, null)(Connection);