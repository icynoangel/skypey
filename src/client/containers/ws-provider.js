import { connect } from 'react-redux';
import WS from '../components/ws/ws';
import { login, setLoggedIn, receiveUsers, setActiveUser } from '../actions/user-actions';
import { setError, setConnectionStatus } from '../actions/ui-actions';
import { receiveMessage } from '../actions/message-actions';

const mapStateToProps = (state) => {
  return {
    activeUser: state.activeUser,
    user: state.user,
    users: state.users,
    ui: state.ui,
    
  };
}

const mapActionToProps = (dispatch) => {
  return {
    login: (id) => {
      login(id);
    },
    setLoggedIn: (user) => {
      dispatch(setLoggedIn(user));
    },
    setActiveUser: (user) => {
      dispatch(setActiveUser(user));
    },
    receiveMessage: (message, user) => {
      dispatch(receiveMessage(message, user));
    },
    receiveUsers: (message, user) => {
      dispatch(receiveUsers(message, user));
    },
    setError: (event) => {
      dispatch(setError(event));
    },
    setConnectionStatus: (status) => {
      dispatch(setConnectionStatus(status));
    }
  };
}

export default connect(mapStateToProps, mapActionToProps)(WS);