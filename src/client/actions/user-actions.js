import {
  SET_ACTIVE_USER,
  SET_LOGGED_IN,
  SET_USERS
} from './actions';

import ws from '../services/ws';

export const setActiveUser = (user) => {
  return {
    type: SET_ACTIVE_USER,
    user
  };
}

export const login = (id) => {
  ws.sendMessage({
    type: 'connect',
    user: {
      id
    }
  });
}

export const setLoggedIn = (user) => {
  return {
    type: SET_LOGGED_IN,
    user
  };
}

export const receiveUsers = (users, user) => {
  return {
    type: SET_USERS,
    users,
    user
  };
}
