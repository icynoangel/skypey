import {
  SHOW_GROUPCHAT_WINDOW
} from './actions';

import ws from '../services/ws';

export const showGroupChatWindow = (status) => {
  return {
    type: SHOW_GROUPCHAT_WINDOW,
    status
  };
}

export const createGroup = (groupName, groupUsers, groupInitiator) => {
  const message = {
    type: 'create-group',
    groupName,
    groupUsers,
    groupInitiator
  };

  ws.sendMessage(message);
}