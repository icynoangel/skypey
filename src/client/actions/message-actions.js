import {
  RECEIVE_MESSAGE,
  SEND_MESSAGE
} from './actions';
import ws from '../services/ws';
import _ from 'lodash';

export const sendMessage = (text, senderId, receiverId) => {
  const message = {
    type: 'message',
    senderId,
    receiverId,
    text,
    id: _.uniqueId(senderId)
  };

  ws.sendMessage(message);

  return {
    type: SEND_MESSAGE,
    message
  };
}

export const receiveMessage = (message, user) => {
  return {
    type: RECEIVE_MESSAGE,
    message,
    user
  };
}