import {
  SET_ERROR,
  SET_CONNECTION_STATUS
} from './actions';

export const setError = (event) => {
  return {
    type: SET_ERROR,
    event
  };
}

export const setConnectionStatus = (status) => {
  return {
    type: SET_CONNECTION_STATUS,
    status
  };
} 
