import React, {Component} from 'react';
import './app.css';
import SidebarProvider from './containers/sidebar-provider';
import MainProvider from './containers/main-provider';
import WSProvider from './containers/ws-provider';
import ConnectionProvider from './containers/connection-provider';
import {Provider} from 'react-redux';
import {createStore} from 'redux';

import reducer from './reducers/reducer';
const store = createStore(reducer);

class App extends Component {

  render() {
    return <Provider store={store}>
      <div className="app">
        <SidebarProvider />
        <MainProvider />
        <ConnectionProvider />
        <WSProvider />
      </div>
    </Provider>
  }
}

export default App;