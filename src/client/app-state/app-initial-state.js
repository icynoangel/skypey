
const appInitialState = {
  user: null,
  activeUser: null,
  users: [],
  /*
 [
   {
     id:,
   }
   {
     groupId:
     groupName:
     groupUsers:
   }
 ] */
  messages: {},

  /*{
    userId1: [
      { type: 
        senderId,
        receiverId,
        message: },
      {},
    ],
    userId2: [
    ],
    groupId1: [
      {
        type:
        senderId: user,
        groupId: groupId
        message:
      }
    ]
  }*/

  groupChatWindow: false,

  ui: {
    error: null,
    connection: {
      connected: false
    }
  }
};

export default appInitialState;