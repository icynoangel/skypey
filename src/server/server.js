const express = require('express');
const http = require('http');
const WebSocket = require('ws');
const chat = require('./chat');
const connection = require('./db/connection');

// create express app
const app = express();
// create http server
const server = new http.createServer(app);

// create websocket server using ws
const wss = new WebSocket.Server({
  server
});

connection.initDB();

wss.on('connection', (ws) => {

  // handle ws messages
  ws.on('message', (message) => {
    let msg;
    try {
      msg = JSON.parse(message);
    } catch (e) {
      console.log("Cannot parse message", e, message);
    }

    if (msg.type === 'connect') {
      try {
        chat.connectUser(ws, msg.user);
      } catch (e) {
        console.log("Cannot connect user", e, msg);
      }
    }
    if (msg.type === 'message') {
      try {
        chat.handleMessage(msg);
      } catch (e) {
        console.log("Cannot handle message", e, msg);
      }
    }
    if (msg.type === 'disconnect') {
      try {
        
        chat.disconnectUser(msg.user);
        ws.terminate();
      } catch (e) {
        console.log("Cannot disconnect user", e, msg);
      }
    }

    if (msg.type === 'create-group') {
      try {
        chat.createGroup(ws, msg);
      } catch (e) {
        console.log('Cannot create group', e, msg);
      }
    }
  });

  ws.on('close', (code, reason) => {
    chat.disconnectUser(ws.user);
  });

});


//start our server
server.listen(8000, () => {
  console.log(`Server started on port ${server.address().port} :)`);
});

