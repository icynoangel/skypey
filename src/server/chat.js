const ChatUser = require('./chat-user');
const ChatGroup = require('./chat-group');
const uid = require('uid');
const _ = require('lodash');

class Chat {

  constructor() {
    this.users = {};
  }

  getConnectedUsers(user) {
    const filteredUsers = _.filter(this.users, (chatUser) => {
      if (chatUser instanceof ChatGroup) {
        if (chatUser.getGroup().groupUsers.indexOf(user.getUser().id) !== -1) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    });

    return _.map(filteredUsers, (chatUser) => {
      if (chatUser instanceof ChatUser) {
        return chatUser.getUser();
      } else {
        return chatUser.getGroup();
      }

    });
  }

  getGroupUsers(groupUsers) {
    return _.map(groupUsers, (id) => {
      return this.users[id];
    });
  }

  connectUser(ws, user) {

    if (_.includes(_.keys(this.users), user.id)) {
      console.log("Same user name");
      ws.send(JSON.stringify({
        "type": "custom-error",
        "message": "User already exists"
      }));

    } else {
      this.users[user.id] = new ChatUser(ws, user);

      this.users[user.id].notifyClient({
        "type": "connected",
        "status": "success",
        "user": user
      });

      this.broadcastUsersList();
    }
  }

  disconnectUser(user) {

    try {

      if (user && this.users[user.id]) {
       
        
        delete this.users[user.id];
        console.log("disconnect user", user);

        _.forEach(this.users, (chatUser) => {
          if (chatUser instanceof ChatGroup) {
           chatUser.removeUser(user.id);
          }
        });

        // console.log(users);
      }
      // this.users = Object.assign(users);
    
    } catch (e) {
      console.log("Cannot disconnect user", e);
    }

    this.broadcastUsersList();
  }

  broadcastUsersList() {

    _.each(this.users, (chatUser) => {

      if (chatUser instanceof ChatUser) {
        chatUser.notifyClient({
          type: 'users',
          users: this.getConnectedUsers(chatUser)
        });
      }

    });
  }

  /**
   * message format
   * {
   *   type: string<connect | message>
   *   senderId: string,
   *   receiverId: string,
   *   text: string,
   *   id: unique-id
   * }
   */
  handleMessage(message) {
    console.log(message);

    if (this.users[message.receiverId] instanceof ChatUser) {

      try {
        this.users[message.senderId].addSentMessage(message);
      } catch (e) {
        console.log("Cannot mark sent message in ChatUser", e);
      }

      try {
        this.users[message.receiverId].addReceivedMessage(message);
      } catch (e) {
        console.log("Cannot mark received message in ChatUser", e);
      }
    } else {
      this.users[message.receiverId].addMessage(message, message.senderId);
    }

  }

  createGroup(ws, message) {
    console.log(message);

    const groupId = uid(10);

    this.users[groupId] = new ChatGroup({
      groupId: groupId,
      groupName: message.groupName,
      groupUsers: this.getGroupUsers(message.groupUsers),
      groupInitiator: message.groupInitiator
    });

    this.broadcastUsersList();
  }

}

module.exports = new Chat();