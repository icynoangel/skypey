const _ = require('lodash');

class ChatGroup {

  constructor(group) {
    this.group = group;
    this.messages = [];
  }

  getGroup() {

    const groupUsers = _.map(this.group.groupUsers, (user) => {
      return user.getUser().id;
    });
    return {
      groupId: this.group.groupId,
      groupName: this.group.groupName,
      groupUsers: groupUsers,
      groupInitiator: this.group.groupInitiator
    };
  }

  removeUser(userId){
    this.group.groupUsers = _.filter(this.group.groupUsers, (user) => {
      if(user.getUser().id === userId){
        return false;
      }
      return true;
    });
  }

  notifyClients(message, senderId) {
    // send data to client
    this.group.groupUsers.forEach(user => {
      if(senderId !== user.getUser().id){
        user.notifyClient(message);
      }
    });
  }

  addMessage(message, senderId) {
    this.notifyClients(message, senderId);
    this.messages.push(message);
  }



}

module.exports = ChatGroup;