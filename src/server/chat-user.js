
class ChatUser {

  constructor(ws, user) {
    this.ws = ws;
    this.ws.user = user;
    this.user = user;
    this.messages = {
      sent: {},
      received: {}
    };
  }

  getUser() {
    return this.user;
  }

  getWS(){
    return this.ws;
  }

  addSentMessage(message) {
    const receiverId = message.receiverId;
    
    if (!this.messages.sent[receiverId]) {
      this.messages.sent[receiverId] = [];
    }
    this.messages.sent[receiverId].push(message);
  }

  addReceivedMessage(message) {
    const senderId = message.senderId;

    if (!this.messages.received[senderId]) {
      this.messages.received[senderId] = [];
    }
    this.messages.received[senderId].push(message);

    this.notifyClient(message);
  }

  notifyClient(message) {
    // send data to client
    this.ws.send(JSON.stringify(message));
  }

}

module.exports = ChatUser;