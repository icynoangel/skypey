const mongoose = require('mongoose');
const UserModel = require('./models/user');

function initDB() {

  mongoose.connect('mongodb://localhost/skypey');

  const db = mongoose.connection;

  db.on('error', (error) => {
    console.log('connection error:', error);
  });

  db.once('open', function () {

    // we're connected!
    console.log("DB connected!");

    //Insert
    UserModel.create({name:'user55'});

    //Query
    UserModel.find({name: /^Io/})
        .then(res => {
          console.log(res)
        })
        .catch(err => {
          console.log(err)
        });
  
  });

}

module.exports = {
  mongoose,
  initDB
};