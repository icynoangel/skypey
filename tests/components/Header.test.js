import React from 'react';
import { shallow } from 'enzyme';
import Header from '../../src/components/main/chatWindow/chatWindowComponents/header';

describe('<Header />', function (){

    this.getComponent = () => {
        return <Header user={this.activeUser} />;
    }

    beforeEach(() => {
        this.activeUser = {
            name : 'Test Name',
            status : 'Test Status'
        };
    })

    it('Should render <Header /> username ', () => {
        const wrapper = shallow(this.getComponent());

        const h2 = wrapper.find('h2');
        expect(h2.text()).to.equal('Test Name');
    });
});